# -*- coding: utf-8 -*-
"""
    Flaskr
    ~~~~~~

    A microblog example application written as Flask tutorial with
    Flask and sqlite3.

    :copyright: (c) 2015 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""

import os
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash

# create our little application :)
app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'foosball.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


photosets={}

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    """Initializes the database."""
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


#@app.cli.command('initdb')
#def initdb_command():
#    """Creates the database tables."""
#    init_db()
#    print('Initialized the database.')


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route('/')
def show_entries():
    db = get_db()
    cur = db.execute('select id_user, name from users order by name asc')
    users = cur.fetchall()  
    
   # if session.get('logged_in'):
    return render_template('score.html',users=users)

@app.route('/stats')
def stats():
    db = get_db()
    cur = db.execute('''select users.name as name, ywg.ywg as yellow_wg, bwg.bwg as black_wg, (ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0)) as won_games,yg.yg as yg, bg.bg as bg, (ifnull(yg.yg,0)+ifnull(bg.bg,0)) as total_games, ((ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0))*1.0/(ifnull(yg.yg,0)+ifnull(bg.bg,0))) as ranking from users
    left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as ywg from (select yellow_p1,count(*) as wg from games where yellow_goals=5 group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_goals=5 and yellow_p1<>yellow_p2 group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) ywg 
        on users.id_user=ywg.player
    left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bwg from (select black_p1,count(*) as wg from games where black_goals=5 group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_goals=5 and black_p1<>black_p2 group by black_p2) p2 on p1.black_p1=p2.black_p2) bwg
        on users.id_user=bwg.player
    left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as yg from (select yellow_p1,count(*) as wg from games group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_p1<>yellow_p2 group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) yg 
        on users.id_user=yg.player
    left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bg from (select black_p1,count(*) as wg from games group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_p1<>black_p2 group by black_p2) p2 on p1.black_p1=p2.black_p2) bg
        on users.id_user=bg.player
        order by ranking desc''')
    alltime = cur.fetchall()  
    cur = db.execute('''select users.name as name, ywg.ywg as yellow_wg, bwg.bwg as black_wg, (ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0)) as won_games,yg.yg as yg, bg.bg as bg, (ifnull(yg.yg,0)+ifnull(bg.bg,0)) as total_games, ((ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0))*1.0/(ifnull(yg.yg,0)+ifnull(bg.bg,0))) as ranking from users
	left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as ywg from (select yellow_p1,count(*) as wg from games where yellow_goals=5 and date>date('now','-7 days') group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_goals=5 and yellow_p1<>yellow_p2 and date>date('now','-7 days') group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) ywg 
		on users.id_user=ywg.player
	left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bwg from (select black_p1,count(*) as wg from games where black_goals=5 and date>date('now','-7 days') group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_goals=5 and black_p1<>black_p2 and date>date('now','-7 days') group by black_p2) p2 on p1.black_p1=p2.black_p2) bwg
		on users.id_user=bwg.player
	left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as yg from (select yellow_p1,count(*) as wg from games where date>date('now','-7 days') group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_p1<>yellow_p2 and date>date('now','-7 days') group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) yg 
		on users.id_user=yg.player
	left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bg from (select black_p1,count(*) as wg from games where date>date('now','-7 days') group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_p1<>black_p2 and date>date('now','-7 days') group by black_p2) p2 on p1.black_p1=p2.black_p2) bg
		on users.id_user=bg.player
		order by ranking desc''')
    lastweek = cur.fetchall()
    cur = db.execute('''select users.name as name, ywg.ywg as yellow_wg, bwg.bwg as black_wg, (ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0)) as crawl_games,yg.yg as wg, bg.bg as bg, (ifnull(yg.yg,0)+ifnull(bg.bg,0)) as total_games, ((ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0))*1.0/(ifnull(yg.yg,0)+ifnull(bg.bg,0))) as ranking from users
	left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as ywg from (select yellow_p1,count(*) as wg from games where yellow_goals=0 and black_goals=5 group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_goals=0 and yellow_p1<>yellow_p2 and black_goals=5 group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) ywg 
		on users.id_user=ywg.player
	left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bwg from (select black_p1,count(*) as wg from games where black_goals=0 and yellow_goals=5 group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_goals=0 and black_p1<>black_p2 and yellow_goals=5 group by black_p2) p2 on p1.black_p1=p2.black_p2) bwg
		on users.id_user=bwg.player
	left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as yg from (select yellow_p1,count(*) as wg from games group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_p1<>yellow_p2 group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) yg 
		on users.id_user=yg.player
	left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bg from (select black_p1,count(*) as wg from games group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_p1<>black_p2 group by black_p2) p2 on p1.black_p1=p2.black_p2) bg
		on users.id_user=bg.player
		order by crawl_games desc''')
    crawler = cur.fetchall()
    cur = db.execute('''	select users.name as name, ywg.ywg as yellow_wg, bwg.bwg as black_wg, (ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0)) as crawl_games,yg.yg as wg, bg.bg as bg, (ifnull(yg.yg,0)+ifnull(bg.bg,0)) as total_games, ((ifnull(ywg.ywg,0)+ifnull(bwg.bwg,0))*1.0/(ifnull(yg.yg,0)+ifnull(bg.bg,0))) as ranking from users
	left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as ywg from (select yellow_p1,count(*) as wg from games where yellow_goals=5 and black_goals=0 group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_goals=5 and yellow_p1<>yellow_p2 and black_goals=0 group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) ywg 
		on users.id_user=ywg.player
	left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bwg from (select black_p1,count(*) as wg from games where black_goals=5 and yellow_goals=0 group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_goals=5 and black_p1<>black_p2 and yellow_goals=0 group by black_p2) p2 on p1.black_p1=p2.black_p2) bwg
		on users.id_user=bwg.player
	left join (select p1.yellow_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as yg from (select yellow_p1,count(*) as wg from games group by yellow_p1) p1 left join (select yellow_p2,count(*) as wg from games where yellow_p1<>yellow_p2 group by yellow_p2) p2 on p1.yellow_p1=p2.yellow_p2) yg 
		on users.id_user=yg.player
	left join (select p1.black_p1 as player, (ifnull(p1.wg,0)+ifnull(p2.wg,0)) as bg from (select black_p1,count(*) as wg from games group by black_p1) p1 left join (select black_p2,count(*) as wg from games where black_p1<>black_p2 group by black_p2) p2 on p1.black_p1=p2.black_p2) bg
		on users.id_user=bg.player
		order by crawl_games desc''')
    victor = cur.fetchall()

        
   # if session.get('logged_in'):
    return render_template('stats.html',alltime=alltime, lastweek=lastweek, crawler=crawler, victor=victor)


@app.route('/record_game')
def record_game():
 #   if not session.get('logged_in'):
 #       abort(401)
    yellow_p1 = request.args.get('yellow_p1', 0, type=int)    
    yellow_p2 = request.args.get('yellow_p2', 0, type=int)    
    black_p1 = request.args.get('black_p1', 0, type=int)    
    black_p2 = request.args.get('black_p2', 0, type=int)    
    black_goals = request.args.get('black_goals', 0, type=int)    
    yellow_goals = request.args.get('yellow_goals', 0, type=int)    
    db = get_db()
    db.execute('insert into games (yellow_p1,yellow_p2,black_p1,black_p2,yellow_goals,black_goals) values (?, ?, ?,?, ?, ?)',
               [yellow_p1,yellow_p2,black_p1,black_p2,yellow_goals,black_goals])
    db.commit()
#    return show_entries()
    return redirect(url_for('show_entries'))



@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
	print request.form['username']
        db = get_db()
        cur = db.execute('select id_user,password from users where username=?',(request.form['username'],))
        password = cur.fetchone()  
	print password 
	if password==None:
	    error='Invalid user'
	else:
            if request.form['password'] != password[1]:
                error = 'Invalid username or password'
       # elif request.form['password'] != app.config['PASSWORD']:
       #     error = 'Invalid password'
            else:
                session['logged_in'] = True
		session['id_user']=password[0]
                flash('You were logged in')
                return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))


if __name__ == '__main__':
    app.run('0.0.0.0',port=80)
