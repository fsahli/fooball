drop table if exists matches;
create table games (
  id_match integer primary key autoincrement,
  'date' timestamp DEFAULT CURRENT_TIMESTAMP,
  yellow_p1 integer not null,
  yellow_p2 integer not null,
  black_p1 integer not null,
  black_p2 integer not null,
  yellow_goals integer not null,
  black_goals integer not null
);
drop table if exists users;
create table users (
  id_user integer primary key autoincrement,
  'date' timestamp DEFAULT CURRENT_TIMESTAMP,
  username text not null,
  name text not null,
  password text not null
);
