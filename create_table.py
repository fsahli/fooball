import sqlite3
from collections import defaultdict

data=defaultdict(list)

conn=sqlite3.connect('foosball.db')
db=conn.cursor()

cur=db.execute('select id_user,name from users')

users=cur.fetchall()

for user in users:
	data['name'].append(user[1])
	cur=db.execute('select count(*) from games where (yellow_p1=? or yellow_p2=?) and yellow_goals=5',(user[0],user[0]))
	data['W_yellow'].append(cur.fetchone()[0])
	cur=db.execute('select count(*) from games where (yellow_p1=? or yellow_p2=?)',(user[0],user[0]))
	data['PG_yellow'].append(cur.fetchone()[0])
	cur=db.execute('select count(*) from games where (black_p1=? or black_p2=?) and black_goals=5',(user[0],user[0]))
	data['W_black'].append(cur.fetchone()[0])
	cur=db.execute('select count(*) from games where (black_p1=? or black_p2=?)',(user[0],user[0]))
	data['PG_black'].append(cur.fetchone()[0])
	data['PG'].append(data['PG_yellow'][-1]+data['PG_black'][-1])
	data['W'].append(data['W_yellow'][-1]+data['W_black'][-1])

	cur=db.execute('select sum(yellow_goals) from games where (yellow_p1=? or yellow_p2=?)',(user[0],user[0]))
	goals= cur.fetchone()[0] if cur.fetchone() is not None else 0
	data['Goals_yellow'].append(goals)
	cur=db.execute('select sum(black_goals) from games where (black_p1=? or black_p2=?)',(user[0],user[0]))
	goals= cur.fetchone()[0] if cur.fetchone() is not None else 0
	data['Goals_black'].append(goals)
	data['Goals'].append(data['Goals_yellow'][-1]+data['Goals_black'][-1])

print data
